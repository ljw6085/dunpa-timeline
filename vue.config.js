module.exports = {
	transpileDependencies: ['vuetify'],
	// 개발 서버 설정
	devServer: {
		// 프록시 설정
		proxy: {
			'^/api/': {
				target: 'https://api.neople.co.kr/',
				pathRewrite: { '^/api/': '' },
				changeOrigin: true,
			},
			'/img/api/': {
				target: 'https://img-api.neople.co.kr/',
				pathRewrite: { '^/img/api/': '' },
				changeOrigin: true,
			},
		},
	},
};
