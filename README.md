# 던전앤파이터 타임라인 통계 조회

## Spec	
 - vue-cli
 - vuetify
 - axios
 - eslint/prettier
---
## 특이사항
 - 빠르고 간결한 결과물을 위하여 백엔드작업을 피하였고, 웹서버 ProxyPass를 이용하여 Front단에서 작업하였습니다.
 - 빠르게 결과물을 내기위해 Vue/Vuetify 프레임워크를 사용하였습니다.
 - 최소한의 기능에 집중하기위해 Nuxt나 vue-route 등 추가적인 라이브러리는 사용하지 않았습니다.
 - 단일기능에 집중하여 최대한 기능별로 나누었으며, 크게 5가지 기능으로 분류하였습니다.
 - `게임서버정보 조회`, `캐릭터 조회조건 관리`, `캐릭터 정보 조회`, `캐릭터 상세정보 조회`, `캐릭터 타임라인 정보 조회`로 나누었고 각각 컴포넌트가 그 기능을 수행합니다.
 - API 호출관련한 함수는 `apiManager.js`파일에 플러그인형태로 제작하여 API통신에 대한 관리포인트를 한곳으로 모았습니다.
 - 소스코드는 prettier를 이용하여 자동정렬 하였고, `탭(4칸)사용`과, `한 줄 최대 100자길이제한` 등을 사용하였습니다. 자동정렬을 적용했다보니 의도치 않은곳에서 엔터가 쳐져있을 수 있는점 감안해주시면 감사하겠습니다.
<br></br><br></br>




# 어플리케이션 실행 방법
## 1. (Windows용) 현재 프로젝트에 포함된 Nginx를 이용하여 바로 실행
 - 방법 1. cmd 에서 직접 실행	
```bash
 # 1. Nginx 실행
 > .\nginx-1.20.1\nginx.exe -p .\nginx-1.20.1
 # 2. 실행 후 http://localhost:80 으로 접속
 # 3. Nginx 종료
 > .\nginx-1.20.1\nginx.exe -s stop -p .\nginx-1.20.1
```
 - 방법 2. bat파일 실행
	- `nginx-start.bat` : nginx 실행
	- `nginx-stop.bat` : nginx 종료
	- http://localhost:80 접속 
 > `dist` 폴더에 소스빌드 결과물이 있고, `nginx`는 `dist`폴더를 기준으로 실행되고 있습니다.  
 > `API Key`는 제가 발급받은 key로 빌드하여 실행되오니, `API Key`를 변경하고자 하시면  
 > `apiManager.js` 파일에서 API key를 변경 후 재빌드하시고 실행 해주세요.  
- 빌드 커맨드
```bash
$ npm install # 관련 라이브러리설치
$ npm run build # 빌드
```
<br></br><br></br>


## 2. 로컬에서 devServer 사용
#### 2-1. Node 환경에서 실행 가능하므로 node 설치 후 버전확인
```
$ node -v
```
#### 2-2. 소스폴더(package.json 경로)에서 필수 라이브러리 설치
```
$ npm install
```

#### 2-3. devServer를 이용하여 실행
```
$ npm run serve
```
> `API Key`세팅이 제가 발급받은 key로 세팅되어 있으므로, `API Key`를 변경하고자 하시면  
> `apiManager.js` 파일에서 API key를 변경 후 재빌드하시고 실행 해주세요.  
<br></br><br></br>


---
## 3. 본인이 제공하는 URL에서 직접 실행(구름IDE 컨테이너)
#### 3-1. 구름IDE 임시 URL로 접속하여 확인
- https://dunpa-api-bjzja.run.goorm.io/
#### 3-2. 만약 404오류가 뜬다면 아래의 공유링크로 Shell 접속
- https://goor.me/Es4qw/
#### 3-3. 접속 후 컨테이너 Shell에서 `nginx` 실행
```bash
# nginx 실행
$ service nginx stop
# nginx 종료
$ service nginx start
```
### 3-4. 다시 `3-1`.번의 URL( https://dunpa-api-bjzja.run.goorm.io/ )로 접속하여 확인.  
<br></br><br></br>



---
# 디렉토리 구조

```bash
  root
	├─ dist                         -- # 소스 빌드결과물
	├─ nginx-1.20.1                 -- # Windows용 Nginx
	├─ nginx-start.bat              -- # nginx실행
	├─ nginx-stop.bat               -- # nginx종료
	├─ README.md                    -- # 프로젝트 기본 정보 파일
	├─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─ ─
	├─ public
	│  └─ index.html                -- # index 템플릿 파일
	├─ src
	│  ├─ App.vue                   -- # Main Component
	│  ├─ main.js                   -- # EntryPoint JS
	│  ├─ components
	│  │  ├─ SearchBar.vue          -- # 캐릭터 목록조회 조건관리 컴포넌트
	│  │  ├─ CharacterList.vue      -- # 캐릭터 목록조회 컴포넌트
	│  │  ├─ CharacterDetail.vue    -- # 캐릭터 상세정보 조회 컴포넌트
	│  │  └─ CharacterTimeline.vue  -- # 캐릭터 타임라인 조회 컴포넌트
	│  └─ plugins
	│     ├─ apiManager.js          -- # API 호출 관리 플러그인
	│     └─ vuetify.js             -- # Vuetify 플러그인 등록
	├─ babel.config.js              -- # 바벨 기본 설정 파일
	├─ package.json                 -- # npm 관리 정보 파일
	└─ vue.config.js                -- # 개발환경 관리파일(개발서버 프록시설정)
```
