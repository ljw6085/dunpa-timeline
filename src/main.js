import Vue from 'vue';
import App from './App.vue';
import axios from 'axios';
import vuetify from './plugins/vuetify';
import apiManager from './plugins/apiManager';

Vue.prototype.$axios = axios;
Vue.prototype.$apiManager = apiManager;

Vue.config.productionTip = false;

new Vue({
	vuetify,
	render: h => h(App),
}).$mount('#app');
