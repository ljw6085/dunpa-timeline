/* eslint-disable no-useless-catch */
import axios from 'axios';
const apikey = 'NIFA4p7DmEYUwfg0Fz3GRAUcgixl5ppJ';
const imgPrefix = '/img';
const prefix = '/api';
// API 관리 객체
export default {
	/**
	 * 서버정보조회
	 * @returns response;
	 */
	async getServerList() {
		try {
			const url = `${prefix}/df/servers`;
			const response = await axios.get(url, {
				params: {
					apikey,
				},
			});
			return response;
		} catch (e) {
			throw e;
		}
	},
	/**
	 * 캐릭터 조회
	 */
	async getCharacter(options) {
		try {
			// 파라미터 세팅
			let serverId = options.serverId;
			if (!serverId) serverId = 'all';

			if (!options.characterName) {
				// eslint-disable-next-line no-throw-literal
				throw '캐릭터명을 입력하세요';
			}
			if (
				options.wordType === 'full' &&
				(options.characterName.length < 2 || options.characterName.length > 12)
			) {
				// 2~12자
				// eslint-disable-next-line no-throw-literal
				throw '전문 검색시 최소 2자에서 최대 12자까지 이용 가능합니다';
			}
			const params = { apikey, characterName: options.characterName };
			if (options.wordType) params.wordType = options.wordType;
			if (options.jobId) params.jobId = options.jobId;
			if (options.jobGrowId) params.jobGrowId = options.jobGrowId;
			if (options.limit) params.limit = options.limit;

			const url = `${prefix}/df/servers/${serverId}/characters`;
			const response = await axios.get(url, {
				params,
			});
			return response;
		} catch (e) {
			throw e;
		}
	},
	/**
	 * 캐릭터 정보 상세조회
	 */
	async getCharacterInfo(serverId, characterId) {
		try {
			const url = `${prefix}/df/servers/${serverId}/characters/${characterId}`;
			const response = await axios.get(url, {
				params: {
					apikey,
				},
			});
			return response;
		} catch (e) {
			throw e;
		}
	},
	/**
	 * 캐릭터 이미지 조회
	 */
	async getCharacterImage(serverId, characterId, zoom = 1) {
		try {
			const url = `${imgPrefix}${prefix}/df/servers/${serverId}/characters/${characterId}`;
			const response = await axios.get(url, {
				params: {
					zoom,
				},
				responseType: 'blob',
			});
			return response;
		} catch (e) {
			throw e;
		}
	},
	/**
	 * 캐릭터 타임라인 조회
	 */
	async getCharacterTimeline(options) {
		try {
			if (!options.characterId) {
				// eslint-disable-next-line no-throw-literal
				throw '캐릭터ID는 필수 파라미터 입니다. ';
			}
			if (!options.serverId) {
				// eslint-disable-next-line no-throw-literal
				throw '서버ID는 필수 파라미터 입니다. ';
			}
			const params = { apikey };
			if (options.startDate) params.startDate = options.startDate;
			if (options.endDate) params.endDate = options.endDate;
			if (options.limit) params.limit = options.limit;
			if (options.code) params.code = options.code;
			if (options.next) params.next = options.next;
			const url = `${prefix}/df/servers/${options.serverId}/characters/${options.characterId}/timeline`;
			const response = await axios.get(url, {
				params,
			});
			return response;
		} catch (e) {
			throw e;
		}
	},
};
